-- sudokuSolver.lua
require("PossibilitySet")
require("Grid")

-- seed the RNG with the current os time
-- so generated puzzles are not always the same
math.randomseed(os.time())

local minimumCells = 39
local removedCellVariance = 21

function importPuzzle(p)
    if type(p) ~= "table" then print("importPuzzle: table needed as arg") return end 
    local g = Grid:new()
    for i=1,81 do g:set(i,p[i]) end
    return g
end

local g = importPuzzle{0,0,0, 0,0,0, 0,0,0,
                       0,0,0, 0,0,0, 0,0,0,
                       0,0,0, 0,0,0, 0,0,0,
                       0,0,0, 0,0,0, 0,0,0,
                       0,0,0, 0,0,0, 0,0,0,
                       0,0,0, 0,0,0, 0,0,0,
                       0,0,0, 0,0,0, 0,0,0,
                       0,0,0, 0,0,0, 0,0,0,
                       0,0,0, 0,0,0, 0,0,0}
                       
local file = assert(io.open("newpuzzle.txt", "w"))
file:write("puzzle = -- do not change this top line!\n")
g:solve()

-- choose random number of cells to eliminate (40-60)
local eliminate = minimumCells + math.random(removedCellVariance)

repeat
    local cell = math.random(41)
    if g[cell].actual ~= 0 then
        if cell == 41 then -- center cell
            g[cell].actual = 0
            eliminate = eliminate - 2
        else
            g[cell].actual = 0
            g[82-cell].actual = 0 -- mirror cell
            eliminate = eliminate - 2
        end
    end
until eliminate <= 0

t = importPuzzle(g:export())
if t:solve() then 
    print()
    print(g)
    file:write(g:toTableString())
else
    print("Alert! Unsolvable puzzle created.")
end
