-- grid.lua
module(..., package.seeall)
require("PossibilitySet")
require("Cell")

local debugOn = false

function Grid:new(object)
    object = object or {}
    for i=1,81 do object[i] = Cell:new{resolved=false, actual=0, possible=PossibilitySet:new{true, true, true, true, true, true, true, true, true}} end
    setmetatable(object, self)
    self.__index = self
    return object
end

function Grid:copy(object)
    object = Grid:new()
    for i=1,81 do 
        object[i] = self[i]:copy()
    end
    setmetatable(object, getmetatable(self))
    return object
end

function Grid:__tostring()
    local str = "-------------------\n"
    for i=1,9 do
        str = string.format("%s%d %d %d  %d %d %d  %d %d %d\n", str, 
                            self[(i-1)*9+1].actual, self[(i-1)*9+2].actual, self[(i-1)*9+3].actual, 
                            self[(i-1)*9+4].actual, self[(i-1)*9+5].actual, self[(i-1)*9+6].actual, 
                            self[(i-1)*9+7].actual, self[(i-1)*9+8].actual, self[(i-1)*9+9].actual)
    end
    str = str.."-------------------\n"
    return str
end

--------------------------------------------------------
-- returns a string bookended by curly brackets
--------------------------------------------------------
function Grid:toTableString()
    local str = string.format("{\n%s}", tostring(self))
   return str
end

function Grid:export()
    local t = {}
    for i=1,81 do
        t[i] = self[i].actual
    end
    return t
end

--------------------------------------------------------
-- enable debug messages
--------------------------------------------------------
function Grid:debug(on)
    if on then debugOn = true
    else debugOn = false end
end

--------------------------------------------------------
-- return a cell object at the specified coordinates
-- or index
--------------------------------------------------------
function Grid:get(y,x)
    return self[((x-1)*9)+y]
end

----------------------------------------------------------------------
-- argument overloaded: can be passed either a column, row and value
-- or a grid index and a value
----------------------------------------------------------------------
function Grid:set(y,x,value)
    if x == nil then return end
    
    local key
    if value == nil then 
        key=y 
        self[key].actual = x
        if self[key].actual ~= 0 then
            self[key].resolved = true
        end
    else key=((x-1)*9)+y
        self[key].actual = value
        if self[key].actual ~= 0 then
            self[key].resolved = true
        end
    end
    if not self[key].resolved then
        for i=1,9 do self:insertPossibility(key,i) end
    else
        for i=1,9 do self:removePossibility(key,i) end
    end
end

function Grid:getPossibilitySet(y,x)
    return self[((x-1)*9)+y].possible
end

--------------------------------------------------------------------
-- inserts a value to a cells possibility set
-- argument overloaded: can be passed either a column, row and value
-- or a grid index and a value
--------------------------------------------------------------------
function Grid:insertPossibility(y,x,value)
    if x == nil then return end
    
    local key
    if value == nil
    then key=y
         self[key].possible[x] = true
    else key=((x-1)*9)+y
         self[key].possible[value] = true
    end
end

----------------------------------------------------------------------
-- removes a value from a cells possibility set
-- argument overloaded: can be passed either a column, row and value
-- or a grid index and a value
----------------------------------------------------------------------
function Grid:removePossibility(y,x,value)
    if x == nil then return end
    
    if value == nil then 
        self[y].possible[x] = nil
    else 
        self[((x-1)*9)+y].possible[value] = nil
    end
end

--------------------------------------------------------
-- checks if the entire puzzle has been solved
-- (a value has been assigned to every cell)
--------------------------------------------------------
function Grid:isComplete()
    for i=1,81 do
        if not self[i].resolved then return false end
    end
    return true
end

--------------------------------------------------------
-- simple solve checks if any cells have only a single 
-- possible value in its possibility set and assigns
-- that as the actual value of the cell
--------------------------------------------------------
function Grid:simpleSolve()
    local p, solvedACell, solvedNum, thiscol, thisrow
    solvedACell = false
    repeat
        solvedNum = 0
        for col=1,9 do
            for row=1,9 do
                -- column check
                for thiscol=1,9 do
                    p = self:get(thiscol, row)
                    if p.resolved then self:removePossibility(col, row, p.actual) end
                end
              -- row check
                for thisrow=1,9 do
                    p = self:get(col, thisrow)
                    if p.resolved then self:removePossibility(col, row, p.actual) end
                end
                -- minigrid check
                for thiscol=col-((col-1)%3),col-((col-1)%3)+2 do
                    for thisrow=row-((row-1)%3),row-((row-1)%3)+2 do
                        p = self:get(thiscol, thisrow)
                        if p.resolved then self:removePossibility(col,row,p.actual) end
                    end
                end

                -- if there's only one possible value for this cell 
                -- set the cell to that value
                local str = tostring(self:getPossibilitySet(col, row))
                if str ~= nil and #str == 1 then
                    self:set(col, row, tonumber(str))
                    solvedNum = solvedNum + 1
                    solvedACell = true
                else 
                    if #str == 0 and not self:get(col,row).resolved then
                        if debugOn then
                            print(string.format("ABORT!!! %d,%d has no possible or actual values", col, row)) 
                        end 
                        return false, "error"
                    end
                end
            end
        end
        if debugOn then 
            print("simple solved="..solvedNum) 
        end
    until solvedNum == 0
    return solvedACell
end

--------------------------------------------------------
-- checks all cells to see if there are cells where it 
-- is the only possible cell in that row that has a 
-- possible value and assign it to that cell if so
--------------------------------------------------------
function Grid:lonePossibilitySetRowSolve()
    local p, solvedACell, solvedNum, count
    solvedACell = false
    solvedNum = 0

    for col=1,9 do
        for p=1,9 do
            count = 0
            for row=1,9 do
                if self:getPossibilitySet(col, row)[p] then
                    count = count + 1
                    if count > 1 then break end
                    y = col
                    x = row
                end
            end
            if count == 1 then
                if debugOn then print(string.format("row solved %d,%d to %d", y, x, p)) end
                self:set(y,x,p)
                solvedACell = true
                solvedNum = solvedNum + 1
            end
        end
    end
    if debugOn then print(string.format("lone possible row solved=%d", solvedNum)) end
    return solvedACell
end

--------------------------------------------------------
-- checks all cells to see if there are cells where it 
-- is the only possible cell in that column that has a 
-- possible value and assign it to that cell if so
--------------------------------------------------------
function Grid:lonePossibilitySetColumnSolve()
    local p, solvedACell, solvedNum, count
    solvedACell = false
    solvedNum = 0

    for row=1,9 do
        for p=1,9 do
            count = 0
            for col=1,9 do
                if self:getPossibilitySet(col, row)[p] then
                    count = count + 1
                    if count > 1 then break end
                    y = col
                    x = row
                end
            end
            if count == 1 then 
                if debugOn then print(string.format("column solved %d,%d to %d", y, x, p)) end
                self:set(y,x,p)
                solvedACell = true
                solvedNum = solvedNum + 1
            end
        end
    end
    if debugOn then print(string.format("lone possible column solved=%d", solvedNum)) end
    return solvedACell
end

--------------------------------------------------------
-- checks all cells to see if there are any cells where it 
-- is the only possible cell in that minigrid that has a 
-- possible value and assign it to that cell if so
--------------------------------------------------------
function Grid:lonePossibilitySetMinigridSolve()
    local p, solvedACell, solvedNum, count
    solvedACell = false
    solvedNum = 0
    
    -- minigrid check
    for mgCol = 0,2 do
        for mgRow = 0,2 do
            for p = 1,9 do
            count = 0
                for row = (mgRow*3+1), (mgRow*3+3) do
                    for col = (mgCol*3+1), (mgCol*3+3) do
                        if self:getPossibilitySet(col, row)[p] then
                            count = count + 1
                            if count > 1 then break end
                            y = col
                            x = row
                        end
                    end
                end
                if count == 1 then 
                    if debugOn then print(string.format("minigrid solved %d,%d to %d", y, x, p)) end
                    self:set(y,x,p)
                    solvedACell = true
                    solvedNum = solvedNum + 1
                end
            end
        end
    end
    if debugOn then print(string.format("lone possible minigrid solved=%d",solvedNum)) end
    return solvedACell
end

-------------------------------------------------------------------
-- assigns a random value to a cell from its potential set
-- and attempts to solve the grid from there. If that random
-- value fails to solve the puzzle we attmpt to use a different
-- value from its possibility set until all possible values have
-- been tried.
--
-- Note that this function can be recursively called from whithin
-- other brute force attempts and as such we only attempt to
-- use it once all other methods have been exhausted
-------------------------------------------------------------------
function Grid:bruteForceSolve()
    local grid, p
    local pTried = PossibilitySet:new()
    if debugOn then print("BruteForceIt") end
    for i=1,81 do
        -- find cell with possibles
        if not self[i].resolved then
            repeat
                grid = self:copy()
                -- we've tried all possible values and can't find a solution
                if grid[i].possible == pTried then return false end
                repeat 
                    -- choose a random possible value from a cell and set it
                    p = grid[i].possible:getRandom()
                until not pTried[p]
                if debugOn then print(string.format("choosing %d from %s in %d", p, tostring(grid[i].possible), i)) end
                pTried[p] = true
                grid:set(i, p)
            until grid:solve()
            return true, grid
        end
    end
end

--------------------------------------------------------
-- entry point for solving or creating sudoku puzzles
--------------------------------------------------------
function Grid:solve()
    local solvedACell = false
    -- repeat until we have gone through an entire loop
    -- without finding a single new solution to a cell
    repeat
        repeat 
            solvedACell, e = self:simpleSolve() 
        until solvedACell == false
        if e == "error" then return false end
        if self:isComplete() then return true end
        
        solvedACell = self:lonePossibilitySetMinigridSolve()
        if not solvedACell then solvedACell = self:lonePossibilitySetColumnSolve() end
        if not solvedACell then solvedACell = self:lonePossibilitySetRowSolve() end 
    until solvedACell == false
    
    if self:isComplete() then 
        return true
    else -- brute force what we have left 
        complete, g = self:bruteForceSolve()

        if complete then 
            for i=1,81 do self:set(i,g[i].actual) end
            return true 
        end
    end
    return false
end