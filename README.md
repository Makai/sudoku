# README #
Lua scripts to generate and solve 9x9 Sudoku puzzles.

[What is Sudoku](https://en.wikipedia.org/wiki/Sudoku)

## Notes ##

### SudokuSolver ###
Each cell in the sudoku puzzle is assigned either a numeric value between 1 and 9 inclusive or a possibility set of all possible values for that cell. Once a single possibility is found for a cell (comparing rows, columns and minigrids) that value is assigned as that cell's actual value.

If the puzzle is still unsolved after possibilities are compared between all the cells, a brute force method is attempted where cells are assigned a random value from within its possibility set and we make another attempt at finding a solution. 

Passing an invalid sudoku puzzle as an input has currently indeterminate behavior but will likely result in an incorrect solution being presented or no solution at all.

Puzzles with multiple solutions will only return the first valid solution found.

### SudokuMaker ###
This is actually just the solver run on a completely empty puzzle grid to generate a valid random sudoku solution and then removing a variable number of cells from the grid. The sudokuMaker.lua script will export the new puzzle into a newpuzzle.txt file. (This file will be overwritten if the script is run multiple times so be sure to rename it if you wish to keep the puzzle).

### Puzzle format ###
Puzzles are inputted and outputted in lua table format. Zeros represent blank values on the sudoku puzzle that need to be resolved. Example puzzles are provided in the examplepuzzles directory. You may input your own puzzles to the script but they must be passed as one puzzle per file and follow the same format as the provided example puzzles.

### To Run the Solver and Maker ###
* ###  Windows ###
From the command prompt run 

```
#!lua

"lua\lua.exe sudokuSolver.lua" 
```

from the project root.
To solve one of the example puzzles enter the file path and name when prompted.
For example:

```
#!lua

"examplepuzzles\examplepuzzle1.txt"
```
 will solve the first example puzzle.
You can also pass the puzzle file path and name as the first argument to the lua script as such:

```
#!lua

"lua\lua.exe sudokuSolver.lua examplepuzzles\examplepuzzle1.txt"
```




* ### Linux and Mac OSX ###
The solver can be run on Linux and Mac OSX systems but requires you to have the lua 5.1 interpreter
installed. Check [http://www.lua.org/](http://www.lua.org) for more information on how to download and install.