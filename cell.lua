-- cell.lua
module(..., package.seeall)

require("PossibilitySet")

function Cell:new(object)
    object = object or {resolved=false, actual=0, possible=PossibilitySet:new{true, true, true, true, true, true, true, true, true}}
    setmetatable(object, self)
    self.__index = self
    return object
end

function Cell:copy()
    local object = self:new()
    object.resolved = self.resolved
    object.actual = self.actual
    if self.possible then
        object.possible = self.possible:copy()
    end
    setmetatable(object, getmetatable(self))
    return object
end

function Cell:set(value)
    if value == 0 then
        self.actual = 0
        self.resolved = false
    else
        self.actual = value
        self.resolved = true
        possible:exhaust()
    end
end