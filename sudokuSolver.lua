-- sudokuSolver.lua
require("Grid")

function importPuzzle(puzzle)
    if type(puzzle) ~= "table" then
        print("importPuzzle: table needed as arg")
        return
    end

    local grid = Grid:new()
    for i=1,81 do
        grid:set(i,puzzle[i])
    end
    return grid
end

-- file passed as argument
if arg[1] then
    assert(loadfile(arg[1]))()
else
    io.write("Enter filename of puzzle to solve:\n");
    local puzzleFile = (io.read())
    assert(loadfile(puzzleFile))()
end

-- puzzle table loaded from file
local grid = importPuzzle(puzzle)
grid:solve() 
print()
print(grid)