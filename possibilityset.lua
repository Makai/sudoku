-- possibilityset.lua
module(..., package.seeall)

function PossibilitySet:new(object)
    object = object or {}
    setmetatable(object, self)
    self.__index = self
    return object
end

function PossibilitySet.__mul(a,b)
    local c = Possible:new()
    for elem in pairs(a) do
        c[elem] = b[elem]
    end
    return c
end

function PossibilitySet.__eq(a,b)
    for i=1,9 do 
        if a[i] ~= b[i] then return false end
    end
    return true
end

function PossibilitySet:__tostring()
    local str = ""
    for i=1,9 do
        if self[i] then
            str = str..i
        end
    end
    return str
end

function PossibilitySet:copy()
    local object = self:new()
    for i=1,9 do object[i] = self[i] end
    setmetatable(object, getmetatable(self))
    return object
end

function PossibilitySet:exhaust()
    for i=1,9 do
        self[i] = nil
    end
end

function PossibilitySet:getRandom()
    if tostring(self) == "" then return nil end
    local r
    repeat r = math.random(9) until self[r]
    return r
end